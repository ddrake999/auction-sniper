# Auction Sniper

## A Ruby walkthrough of the Auction Sniper application in ["Growing Object Oriented Software, Guided by Tests"](http://www.amazon.com/Growing-Object-Oriented-Software-Guided-Tests/dp/0321503627) by Steve Freeman and Nat Pryce

### Definitions:

- *Item* is something that can be identified and bought.
- *Bidder* is a person or organization that is interested in buying an item.
- *Bid* is a statement that a bidder will pay a given price for an item.
- *Current price* is the current highest bid for the item.
- *Stop price* is the most a bidder is prepared to pay for an item.
- *Auction* is a process for managing bids for an item.
- *Auction house* is an institution that hosts auctions.

### Dependencies:

- *FxRuby* (GUI used in place of Swing): 
  - [FxRuby](http://www.fxruby.org/)
  - [Linux Installation](https://github.com/lylejohnson/fxruby/wiki/Setting-Up-a-Linux-Build-Environment)
- *XMPP4R* An XMPP/Jabber library for Ruby: [XMPP4R](http://home.gna.org/xmpp4r/)
